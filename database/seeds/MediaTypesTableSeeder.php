<?php

use Illuminate\Database\Seeder;

class MediaTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $media_type = new App\MediaType(['name' => 'youtube']);
        $media_type->save();
    }
}
