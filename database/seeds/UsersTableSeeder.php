<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        // Create user with role `admin`
        factory(App\User::class)->create([
            'email' => 'admin@example.com',
            'role_id' => 1
        ]);

        // Create some users with role `judge`
        factory(App\User::class)->create([
            'email' => 'judge@example.com',
            'role_id' => 2
        ]);
        factory(App\User::class, 2)->create(['role_id' => 2]);

        // Create user with role `moderator`
        factory(App\User::class)->create([
            'email' => 'moderator@example.com',
            'role_id' => 3
        ]);

         // Create user with role `supervisor`
        factory(App\User::class)->create([
            'email' => 'supervisor@example.com',
            'role_id' => 4
        ]);
    }
}
