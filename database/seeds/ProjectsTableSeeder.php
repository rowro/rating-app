<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data_nominations = [
            [
                'name' => 'Лучшее шоу',
                'image' => '/storage/public/nomination-1.jpg'
            ],
            [
                'name' => 'Лучший блог о путешествиях',
                'image' => '/storage/public/nomination-2.jpg'
            ],
            [
                'name' => 'Самый жизнеутверждающий блог',
                'image' => '/storage/public/nomination-3.jpg'
            ],
            [
                'name' => 'Лучший музыкальный ролик',
                'image' => '/storage/public/nomination-4.jpg'
            ],
            [
                'name' => 'Самый красивый блог',
                'image' => '/storage/public/nomination-5.jpg'
            ]
        ];

        $data_projects = [
            [
                'title' => 'Мегаполисы России',
                'description' => 'Узнаем, какие зарплаты в Москве, стоит ли там учиться, какие плюсы и минусы у Москвы. Красивое видео о современной Москве',
                'media' => 'https://www.youtube.com/watch?v=S_dfq9rFWAE'
            ],
            [
                'title' => 'Города России',
                'description' => 'Основные туристические достопримечательности и культурная жизнь города, красивая природа . . .',
                'media' => 'https://www.youtube.com/watch?v=xK23RHXXlJ4'
            ],
            [
                'title' => 'Полуостров Крым',
                'description' => 'Полуостров Крым с Высоты птичьего полёта. Туристические достопримечательности, красивая природа . . .',
                'media' => 'https://www.youtube.com/watch?v=7YQB7OytLhA'
            ]
        ];

        $data_criteria = [
            'Идея',
            'Сюжет',
            'Креативность',
            'Музыка'
        ];

        $criteria_count = 4;
        $judges = App\User::where('role_id', 2)->get();
    

        foreach ($data_nominations as $item_data) {
            $nomination = factory(App\Nomination::class)->create($item_data);
            // Добавляем критерии
            for ($i=0; $i < $criteria_count; $i++) {
                $criterion = factory(App\Criterion::class)->create([
                    'name' => $data_criteria[$i]
                ]);
    
                $nomination->criteria()->save($criterion);
            }

            // Добавляем проекты
            $data_projects_index = 0;
            for ($i=0; $i < 12; $i++) {
                $data_projects_index = $data_projects_index != count($data_projects) - 1 ? ++$data_projects_index : 0;

                $project = factory(App\Project::class)->create([
                    'title' => $data_projects[$data_projects_index]['title'],
                    'description' => $data_projects[$data_projects_index]['description'],
                    'nomination_id' => $nomination->id
                ]);

                $media = factory(App\Media::class)->create([
                    'project_id' => $project->id,
                    'link' => $data_projects[$data_projects_index]['media']
                ]);

                // Добавляем оценки
                if ($project->moderated && $i < 5) {
                    foreach ($judges as $judge) {
                        foreach ($nomination->criteria as $criterion) {
                            $rating = new App\Rating([
                                'user_id' => $judge->id,
                                'project_id' => $project->id,
                                'criterion_id' => $criterion->id,
                                'value' => rand(1, 10)
                            ]);
                            $project->ratings()->save($rating);
                        }
                    }
                }
            }
        }
    }
}
