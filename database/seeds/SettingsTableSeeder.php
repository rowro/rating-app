<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $default_settings = [
            'title' => 'БлогФест',
            'description' => 'Приложение для оценки конкурсных работ',
            'reception' => true
        ];

        $settings = new App\Setting($default_settings);
        $settings->save();
    }
}
