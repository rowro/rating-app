<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $roles = [
            ['name' => 'admin', 'label' => 'Администратор'],
            ['name' => 'judge', 'label' => 'Член жюри'],
            ['name' => 'moderator', 'label' => 'Модератор'],
            ['name' => 'supervisor', 'label' => 'Председатель комиссии']
        ];

        foreach ($roles as $role) {
            $new_role = new App\Role([
                'name' => $role['name'],
                'label' => $role['label']
            ]);
            $new_role->save();
        }
    }
}
