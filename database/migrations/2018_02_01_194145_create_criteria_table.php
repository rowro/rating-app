<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCriteriaTable extends Migration
{
    public function up()
    {
        Schema::create('criteria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('nomination_id')->unsigned();
            $table->timestamps();

            $table->foreign('nomination_id')->references('id')->on('nominations')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('criteria');
    }
}
