<?php

use Faker\Generator as Faker;

$factory->define(App\Media::class, function (Faker $faker) {
    return [
        'link' => 'https://www.youtube.com/watch?v=tz4ELHsEWkw&list=PLHRfWmB-cTz9AnFgcu7kZt6vsN0krJWhB',
        'type_id' => 1
    ];
});
