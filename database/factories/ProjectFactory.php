<?php

use Faker\Generator as Faker;

$factory->define(App\Project::class, function (Faker $faker) {

    $nominations = App\Nomination::all()->pluck('id')->all();

    $projects_info = [
        [
            'title' => 'Москва - главный город России.',
            'description' => 'Узнаем, какие зарплаты в Москве, стоит ли там учиться, какие плюсы и минусы у Москвы. Красивое видео о современной Москве',
            'media' => 'https://www.youtube.com/watch?v=S_dfq9rFWAE'
        ],
        [
            'title' => 'Столица Приморского края и Дальнего востока России.',
            'description' => 'Основные туристические достопримечательности и культурная жизнь города, красивая природа Приморского Края c необычных ракурсов, снятые многомоторными летающими платформами.',
            'media' => 'https://www.youtube.com/watch?v=xK23RHXXlJ4'
        ],
        [
            'title' => 'Путешествие туристов по городам и прекрасным местам Крыма.',
            'description' => 'Полуостров Крым с Высоты птичьего полёта.',
            'media' => 'https://www.youtube.com/watch?v=7YQB7OytLhA'
        ]
    ];

    $random_index = $faker->numberBetween($min = 0, $max = count($projects_info) - 1);

    return [
        'title' => $projects_info[$random_index]['title'],
        'description' => $projects_info[$random_index]['description'],
        'nomination_id' => $faker->randomElement($array = $nominations),
        'moderated' => $faker->boolean($chanceOfGettingTrue = 60),
        'member_id' => function () {
            return factory(App\Member::class)->create()->id;
        }
    ];
});
