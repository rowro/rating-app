<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {

    $random_num = $faker->numberBetween($min = 0, $max = 1);
    
    return [
        'name' => ($random_num) ? $faker->firstNameMale : $faker->firstNameFemale,
        'lastname' => ($random_num) ? $faker->lastName : $faker->lastName . 'а',
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
        'role_id' => 2
    ];
});
