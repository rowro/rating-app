<?php

use Faker\Generator as Faker;

$factory->define(App\Member::class, function (Faker $faker) {

    $random_num = $faker->numberBetween($min = 0, $max = 1);

    return [
        'name' => ($random_num) ? $faker->firstNameMale : $faker->firstNameFemale,
        'lastname' => ($random_num) ? $faker->lastName : $faker->lastName . 'а',
        'email' => $faker->unique()->safeEmail,
        'phone' => '+7 (999) 777-77-77',
    ];
});
