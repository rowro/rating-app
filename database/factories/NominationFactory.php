<?php

use Faker\Generator as Faker;

$factory->define(App\Nomination::class, function (Faker $faker) {
    return [
        'name' => $faker->numerify('Номинация ###')
    ];
});
