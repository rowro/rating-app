<?php

use Faker\Generator as Faker;

$factory->define(App\Criterion::class, function (Faker $faker) {

    $nominations = App\Nomination::all()->pluck('id')->all();

    return [
        'name' => $faker->numerify('Критерий ###'),
        'nomination_id' => $faker->randomElement($array = $nominations)
    ];
});
