<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $hidden = ['project_id', 'type_id', 'created_at', 'updated_at'];

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function type()
    {
        return $this->belongsTo('App\MediaType');
    }
}
