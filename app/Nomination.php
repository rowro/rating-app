<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nomination extends Model
{
    protected $hidden = ['pivot'];
    
    public function criteria()
    {
        return $this->hasMany('App\Criterion');
    }

    public function projects()
    {
        return $this->hasMany('App\Project');
    }
}
