<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Member extends Model
{
    use Notifiable;
    
    public function projects()
    {
        return $this->hasMany('App\Project');
    }
}
