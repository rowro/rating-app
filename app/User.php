<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token', 'role_id'
    ];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function ratings()
    {
        return $this->hasMany('App\Rating');
    }
}
