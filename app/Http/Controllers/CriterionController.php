<?php

namespace App\Http\Controllers;

use App\Criterion;
use App\Nomination;
use Illuminate\Http\Request;

class CriterionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Вывод списка критериев.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user()->load('role');

        if ($user->role->name === 'admin') {
            return Criterion::with(['nomination:id,name'])->paginate(10);
        }

        return Criterion::with(['nomination:id,name'])->get();
    }

    /**
     * Добавление нового критерия.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'nomination' => 'required|numeric|exists:nominations,id'
        ]);

        $nomination = Nomination::findOrFail($request->input('nomination'));

        $criterion = new Criterion();
        $criterion->name = $request->input('name');

        $nomination->criteria()->save($criterion);

        return $criterion->load(['nomination']);
    }

    /**
     * Вывод одного критерия.
     *
     * @param  \App\Criterion  $criterion
     * @return \Illuminate\Http\Response
     */
    public function show(Criterion $criterion)
    {
        return $criterion->with(['nomination'])->get();
    }

    /**
     * Редактирование критерия.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Criterion  $criterion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Criterion $criterion)
    {
        $request->validate([
            'name' => 'required',
            'nomination' => 'required|numeric|exists:nominations,id'
        ]);

        $criterion->name = $request->input('name');
        $criterion->nomination_id = $request->input('nomination');
        $criterion->save();

        return $criterion->load(['nomination']);
    }

    /**
     * Удаление критерия.
     *
     * @param  \App\Criterion  $criterion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Criterion $criterion)
    {
        $id = $criterion->id;
        $criterion->delete();

        return $id;
    }
}
