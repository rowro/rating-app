<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Вывод списка пользователей.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $current_user = $request->user()->load('role');

        if ($current_user->role->name === 'admin') {
            return User::with('role')->paginate(10);
        }
    }

    /**
     * Добавление пользователя.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'lastname' => 'required',
            'role' => 'required|numeric|exists:roles,id',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $current_user = $request->user()->load('role');

        if ($current_user->role->name === 'admin') {
            $user = new User();
            $user->name = $request->input('name');
            $user->lastname = $request->input('lastname');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->role_id = $request->input('role');

            $user->save();

            return $user->load('role');
        }
    }

    /**
     * Вывод одного пользователя.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {
        $current_user = $request->user()->load('role');

        if ($current_user->role->name === 'admin') {
            return $user->load('role');
        }
    }

    /**
     * Редактирование пользователя.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'lastname' => 'required',
            'role' => 'required|numeric|exists:roles,id',
            'email' => 'required|email'
        ]);

        $current_user = $request->user()->load('role');

        if ($current_user->role->name === 'admin') {
            $user->name = $request->input('name');
            $user->lastname = $request->input('lastname');
            $user->email = $request->input('email');
            $user->role_id = $request->input('role');

            if ($request->input('password')) {
                $user->password = bcrypt($request->input('password'));
            }

            $user->save();

            return $user->load('role');
        }
    }

    /**
     * Удаление пользователя.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $current_user = $request->user()->load('role');
        
        if ($current_user->role->name === 'admin') {
            $id = $user->id;
            $user->delete();

            return $id;
        }
    }
}
