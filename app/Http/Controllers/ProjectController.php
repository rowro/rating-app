<?php

namespace App\Http\Controllers;

use App\User;
use App\Media;
use App\Member;
use App\Project;
use App\Nomination;
use App\Notifications\ModerationComplete;
use App\Notifications\ModerationFail;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['store']]);
        $this->yotubeRegexp = "/https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*)([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['\"][^<>]*>|<\/a>))[?=&+%\w.-]*/i";
    }

    /**
     * Вывод списка конкурсных работ.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user()->load('role');
        $nomination_id = $request->input('nomination_id');

        if ($user->role->name === 'moderator') {
            if ($nomination_id) {
                $nomination = Nomination::findOrFail($nomination_id);
                return $nomination->projects()
                    ->with('media')
                    ->where('moderated', false)
                    ->get();
            }
            return Project::with('media')->where('moderated', false)->get();
        }

        if ($user->role->name === 'judge') {
            if ($nomination_id) {
                $nomination = Nomination::findOrFail($nomination_id);
                $projects = $nomination
                    ->projects()
                    ->where('moderated', true)
                    ->withCount('ratings')
                    ->with('media')
                    ->get();

                foreach ($projects as $project) {
                    $project['completed'] = $nomination->criteria->count() === $project->ratings_count;
                    unset($project['ratings_count']);
                }
                return $projects;
            }

            return Project::where('moderated', 1)->get();
        }

        if ($user->role->name === 'admin') {
            return Project::with(['member', 'media', 'nomination'])->paginate(10);
        }
    }

    /**
     * Добавление новой конкурсной работы.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:80',
            'description' => 'required|max:140',
            'link' => ['required', 'regex:' . $this->yotubeRegexp],
            'nomination' => 'required|numeric|exists:nominations,id',
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'phone' => 'required'
        ]);

        $member = new Member;
        $member->name = $request->input('name');
        $member->lastname = $request->input('lastname');
        $member->email = $request->input('email');
        $member->phone = $request->input('phone');
        $member->save();

        $project = new Project;
        $project->title = $request->input('title');
        $project->description = $request->input('description');
        $project->nomination_id = $request->input('nomination');
        $project->member_id = $member->id;

        $media = new Media;
        $media->type_id = 1; // YouTube
        $media->link = $request->input('link');

        $project->save();
        $project->media()->save($media);

        return $project->load(['media', 'member', 'nomination']);
    }

    /**
     * Вывод одной конкурсной работы.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Project $project)
    {
        $user = $request->user()->load('role');
        if ($user->role->name === 'supervisor' || $user->role->name === 'moderator') {
            return $project->load(['nomination:id,name', 'media', 'member']);
        }

        $nomination = $project->nomination;
        $criteria_count = $nomination->criteria->count();
        $next_incompleted_project = $nomination->projects()->has('ratings', '<', $criteria_count)->first();

        $project['next_incompleted_project_id'] = ($next_incompleted_project) ? $next_incompleted_project->id : 0;

        $project['completed'] = $criteria_count === $project->ratings->where('user_id', $user->id)->count();
        return $project->load(['nomination:id,name', 'media']);
    }

    /**
     * Вывод рейтинга конкурсной работы.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showRating(Request $request, $id)
    {
        $user_id = $request->user()->id;
        $project = Project::findOrFail($id);

        $criteria = $project->nomination->criteria;
        $ratings = $project->ratings()->where('user_id', $user_id)->get();

        $result = [];
        foreach ($criteria as $criterion) {
            $current_rating = $ratings->first(function ($item) use ($criterion) {
                return $item->criterion_id == $criterion->id;
            });

            $result['rating'][] = [
                'criterion_id' => $criterion->id,
                'name' => $criterion->name,
                'value' => ($current_rating) ? $current_rating->value : 0
            ];
        }

        $result['completed'] = $criteria->count() === $ratings->count();

        return $result;
    }

    /**
     * Сохранение рейтинга конкурсной работы.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveRating(Request $request, $id)
    {
        $user_id = $request->user()->id;
        $project = Project::findOrFail($id);
        $nomination = $project->nomination;

        $response = [
            'project_id' => $project->id,
            'rating' => []
        ];

        foreach ($request->input('ratings') as $rating) {
            $response['ratings'][] = $project->ratings()->updateOrCreate([
                'user_id' => $user_id,
                'project_id' => $project->id,
                'criterion_id' => $rating['criterion_id']
            ], [
                'value' => (int) $rating['value']
            ]);
        }

        $criteria_count = $nomination->criteria->count();
        $next_incompleted_project = $nomination->projects()->has('ratings', '<', $criteria_count)->first();

        $response['next_incompleted_project_id'] = ($next_incompleted_project) ? $next_incompleted_project->id : 0;

        return $response;
    }

    /**
     * Модерация конкурсной работы.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveModeration(Request $request, $id)
    {
        $user_id = $request->user()->id;
        $moderation_status = $request->input('status');
        $nomination = Nomination::findOrFail($request->input('nomination_id'));
        $project = Project::findOrFail($id);

        $member = $project->member;
        
        if ($moderation_status === 'accept') {
            $member->notify(new ModerationComplete($project));
            $project->moderated = true;
            $project->save();
        }
        
        if ($moderation_status === 'reject') {
            $reason = $request->input('comment');
            $member->notify(new ModerationFail($project, $reason));
            $project->delete();
        }

        $response = [
            'project_id' => $id,
            'status' => $moderation_status,
            'comment' => (string) $request->input('comment')
        ];

        $response['next_unmoderated_project_id'] = $nomination->projects()
            ->where('moderated', false)
            ->first()
            ->id;

        return $response;
    }

    /**
     * Редактирование конкурсной работы.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $request->validate([
            'title' => 'required|max:80',
            'description' => 'required|max:140',
            'link' => ['required', 'regex:' . $this->yotubeRegexp],
            'nomination' => 'required|numeric|exists:nominations,id',
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'phone' => 'required'
        ]);

        $member = Member::find($project->member_id);
        $member->name = $request->input('name');
        $member->lastname = $request->input('lastname');
        $member->email = $request->input('email');
        $member->phone = $request->input('phone');
        $member->save();
        
        $media = Media::where('project_id', $project->id)->first();
        $media->link = $request->input('link');
        $media->save();

        $project->title = $request->input('title');
        $project->description = $request->input('description');
        $project->nomination_id = $request->input('nomination');
        $project->save();

        return $project->load(['media', 'member', 'nomination']);
    }

    /**
     * Удаление конкурсной работы.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $id = $project->id;
        $project->delete();

        return $id;
    }
}
