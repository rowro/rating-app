<?php

namespace App\Http\Controllers;

use App\Nomination;
use App\Rating;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NominationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['getShortList']]);
    }

    /**
     * Вывод списка номинаций.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user()->load('role');

        if ($user->role->name === 'admin') {
            return Nomination::paginate(10);
        }

        $nominations = Nomination::with([
                'projects' => function ($q) use ($user) {
                    $q->select('id', 'nomination_id')
                    ->when($user->role->name === 'moderator', function ($query) {
                            $query->where('moderated', false);
                    }, function ($query) {
                            $query->where('moderated', true);
                    });
                }
            ])
            ->withCount([
                'criteria',
                'projects' => function ($q) use ($user) {
                    $q->when($user->role->name === 'moderator', function ($query) {
                        $query->withTrashed();
                    }, function ($query) {
                        $query->where('moderated', true);
                    });
                }
            ])
            ->get();

        foreach ($nominations as $nomination) {
            if ($user->role->name === 'judge') {
                $incompleted_projects = $nomination->projects()
                    ->where('moderated', true)
                    ->whereHas('ratings', function ($q) use ($user) {
                        $q->where('user_id', $user->id);
                    }, '<>', $nomination->criteria->count());

                $first_project = $incompleted_projects->first();
                $nomination->next_incompleted_project_id = ($first_project) ? $first_project->id : 0;
                $nomination->completed_projects_count = $nomination->projects_count - $incompleted_projects->count();
            }
            
            if ($user->role->name === 'moderator') {
                $nomination->completed_projects_count = $nomination->projects_count - $nomination->projects->count();
                $first_project = $nomination->projects->first();
                $nomination->next_unmoderated_project_id = ($first_project) ? $first_project->id : 0;
            }

            unset($nomination['projects']);
            unset($nomination['criteria']);
        }
        
        return $nominations;
    }

     /**
     * Вывод списка номинаций с неполными данными.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getShortList(Request $request)
    {
        return Nomination::select('id', 'name')->get();
    }

    /**
     * Добавление новой номинации.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'required|file|mimes:jpeg,png'
        ]);

        $file = $request->file('image');

        $nomination = new Nomination();

        $nomination->name = $request->input('name');
        $nomination->image = '/storage/' . $file->store('public');
        $nomination->save();

        return $nomination;
    }

    /**
     * Вывод одной номинации.
     *
     * @param  \App\Nomination  $nomination
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Nomination $nomination)
    {
        $user = $request->user()->load('role');

        $nomination->load([
            'criteria',
            'projects' => function ($q) use ($user) {
                $q->select('id', 'nomination_id')
                  ->when($user->role->name === 'moderator', function ($query) {
                        $query->where('moderated', false);
                  }, function ($query) {
                        $query->where('moderated', true);
                  });
            }
            ])
            ->get();

            
        if ($user->role->name === 'judge') {
            $incompleted_projects = $nomination->projects()
            ->where('moderated', true)
            ->whereHas('ratings', function ($q) use ($user) {
                $q->where('user_id', $user->id);
            }, '<', $nomination->criteria->count());
                
            $nomination->projects_count = $nomination->projects->count();
            $nomination->completed_projects_count = $nomination->projects_count - $incompleted_projects->count();
            $nomination->next_incompleted_project_id = ($incompleted_projects->first()) ? $incompleted_projects->first()->id : 0;
        }
        
        if ($user->role->name === 'moderator') {
            $nomination->projects_count = $nomination->projects()->withTrashed()->count();
            $nomination->completed_projects_count = $nomination->projects_count - $nomination->projects->count();
            $first_project = $nomination->projects->first();
            $nomination->next_unmoderated_project_id = ($first_project) ? $first_project->id : 0;
        }

        unset($nomination['projects']);
        unset($nomination['criteria']);
        
        return $nomination;
    }

    /**
     * Вывод критериев к номинации.
     *
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function showCriteria($id)
    {
        return Nomination::findOrFail($id)->criteria;
    }

    /**
     * Редактирование номинации.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nomination  $nomination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Nomination $nomination)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'file|mimes:jpeg,png'
        ]);

        $file = $request->file('image');

        $nomination->name = $request->input('name');
        if ($file) {
            $nomination->image = '/storage/' . $file->store('public');
        }
        $nomination->save();

        return $nomination;
    }

    /**
     * Удаление номинации.
     *
     * @param  \App\Nomination  $nomination
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nomination $nomination)
    {
        $id = $nomination->id;
        $nomination->delete();

        return $id;
    }
}
