<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['exclude' => ['index']]);
    }

    /**
     * Вывод списка настроек.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Setting::find(1);
    }

    /**
     * Сохранение настроек.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $user = $request->user()->load('role');

        if ($user->role->name === 'admin') {
            $request->validate([
                'title' => 'required',
                'reception' => 'boolean'
            ]);

            $settings = Setting::find(1);
            $settings->title = $request->input('title');
            $settings->description = $request->input('description');
            $settings->reception = $request->input('reception');
            $settings->save();
            
            return $settings;
        }
    }
}
