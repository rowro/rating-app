<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(\Illuminate\Http\Request $request, $user)
    {
        if ($request->ajax()) {
            $user = $user->load('role');
            $redirectPath = $this->redirectPath();

            if ($user->role->name === 'admin') {
                $redirectPath = '/admin';
            }

            if ($user->role->name === 'judge' || $user->role->name === 'moderator') {
                $redirectPath = '/nominations';
            }

            if ($user->role->name === 'supervisor') {
                $redirectPath = '/dashboard';
            }

            return response()->json([
                'auth' => auth()->check(),
                'user' => $user,
                'intended' => $redirectPath,
            ]);
        }
    }
}
