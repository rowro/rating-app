<?php

namespace App\Http\Controllers;

use App\User;
use App\Project;
use App\Nomination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatsController extends Controller
{
     /**
     * Вывод статистики и списка номинантов
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $winners = Nomination::select('id', 'name', 'image')
            ->withCount([
                'criteria',
                'projects' => function ($query) {
                    $query->where('moderated', true);
                }
            ])
            ->with(['projects' => function ($query) {
                $query->select('id', 'title', 'nomination_id', 'member_id')
                ->withCount('ratings')
                ->with([
                    'member:id,name,lastname,email,phone',
                    'ratings' => function ($q) {
                        $q->select('user_id', 'project_id', DB::raw('ROUND(AVG(value), 2) as value'))
                            ->groupBy(['project_id', 'user_id']);
                    }
                ])
                ->where('moderated', true)
                ->has('ratings', '>', 0)
                ->limit(8);
            }])
            ->get();

        $total_judges = User::where('role_id', 2)->count();
        $completed_projects_count = 0;

        foreach ($winners as $key => $nomination) {
            foreach ($nomination->projects as $project) {
                $total = 0;
                if ($project->ratings_count === $nomination->criteria_count * $total_judges) {
                    $completed_projects_count += 1;
                }
                foreach ($project->ratings as $rating) {
                    $total += $rating->value;
                    unset($rating['project_id']);
                }
                $project->score = round($total / $project->ratings->count(), 2);
                unset($project['member_id']);
                unset($project['nomination_id']);
            }
        }

        return [
            'moderated_projects_count' => Project::where('moderated', true)->count(),
            'completed_projects_count' => $completed_projects_count,
            'total_projects_count' => Project::withTrashed()->count(),
            'winners' => $winners
        ];
    }
}
