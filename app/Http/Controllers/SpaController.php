<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SpaController extends Controller
{
    /**
     * Загрузка начальных данных.
     */
    public function index(Request $request)
    {
        $settings = Setting::find(1);

        $user = Auth::user();
        $user = ($user) ? $user->load('role') : [];

        $initial_state = [
            'user' => $user,
            'settings' => [
                'reception' => $settings->reception
            ]
        ];

        return view('index', compact('initial_state', 'settings'));
    }
}
