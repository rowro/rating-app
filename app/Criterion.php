<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Criterion extends Model
{
    protected $hidden = ['nomination_id'];

    public function ratings()
    {
        return $this->hasMany('App\Rating');
    }

    public function nomination()
    {
        return $this->belongsTo('App\Nomination');
    }
}
