<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $hidden = ['pivot'];

    public function ratings()
    {
        return $this->hasMany('App\Rating');
    }

    public function media()
    {
        return $this->hasOne('App\Media')->with('type');
    }

    public function nomination()
    {
        return $this->belongsTo('App\Nomination');
    }

    public function member()
    {
        return $this->belongsTo('App\Member');
    }
}
