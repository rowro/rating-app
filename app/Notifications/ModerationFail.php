<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ModerationFail extends Notification
{
    use Queueable;

    protected $project;
    protected $reason;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($project, $reason)
    {
        $this->project = $project;
        $this->reason = $reason;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('К сожалению ваша конкурсная работа не прошла модерацию!')
                    ->greeting('Здравствуйте, '. $this->project->member->name .'!')
                    ->line('К сожалению ваша конкурсная работа "' . $this->project->title . '" не прошла модерацию!')
                    ->line('Причина: ' . $this->reason)
                    ->salutation('Спасибо за участие в нашем фестивале!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
