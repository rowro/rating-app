const path = require('path');
let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .extract(['vue', 'vue-router', 'element-ui']);

mix.stylus('resources/assets/stylus/app.styl', 'public/css', {
  use: [ require('rupture')() ],
  import: [ path.resolve(__dirname, 'resources/assets/stylus/vars.styl') ]
})
.sourceMaps();

if (mix.inProduction()) {
  mix.version();
  mix.disableNotifications();
}

mix.browserSync('rating-app');

mix.webpackConfig({
  module: {
    rules: [{
      enforce: 'pre',
      test: /\.(js|vue)$/,
      loader: 'eslint-loader',
      exclude: /node_modules/
    }]
  },
  resolve: {
    alias: {
      Components: path.resolve(__dirname, 'resources/assets/js/components')
    }
  }
});