import Vue from 'vue';
import Vuex from 'vuex';
import * as actions from './actions';
import mutations from './mutations';

Vue.use(Vuex);

const state = {
  user: {},
  stats: {},
  rating: [],
  project: {},
  nomination: {},
  settings: {},
  userList: [],
  projectList: [],
  criterionList: [],
  nominationList: [],
  pagination: {
    userList: { currentPage: 1, total: 1 },
    projectList: { currentPage: 1, total: 1 },
    criterionList: { currentPage: 1, total: 1 },
    nominationList: { currentPage: 1, total: 1 },
  },
  ...window.App.initialState
};

const store = new Vuex.Store({
  state,
  mutations,
  actions,
});

export default store;
