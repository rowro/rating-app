// Номинации
export const loadNominationList = async ({ commit }, { short = false, page = 1 } ) => {
  let paramShortList = short ? '/short' : '';
  commit('LOAD_NOMINATION_LIST', await axios.get(`/api/nominations${paramShortList}`, {
    params: {
      page
    }
  }));
}

export const loadNomination = async ({ commit }, id) => {
  commit('LOAD_NOMINATION', await axios.get(`/api/nominations/${id}`));
}

export const createNomination = async ({ commit }, { name, image }) => {
  let data = new FormData();
  data.append('name', name);
  data.append('image', image);

  commit(
    'CREATE_NOMINATION',
    await axios.post(`/api/nominations`, data)
  );
}

export const updateNomination = async ({ commit }, { id, name, image = null }) => {
  let data = new FormData();
  data.append('name', name);
  data.append('image', image);
  data.append('_method', 'PATCH');

  commit(
    'UPDATE_NOMINATION',
    await axios.post(`/api/nominations/${id}`, data)
  );
}

export const deleteNomination = async ({ commit }, id) => {
  commit(
    'DELETE_NOMINATION',
    await axios.delete(`/api/nominations/${id}`)
  );
}

// Критерии
export const loadCriterionList = async ({ commit }, { page }) => {
  commit(
    'LOAD_CRITERION_LIST', 
    await axios.get('/api/criteria', {
      params: { page }
    })
  );
}

export const createCriterion = async ({ commit }, data) => {
  commit(
    'CREATE_CRITERION',
    await axios.post(`/api/criteria`, { ...data })
  );
}

export const updateCriterion = async ({ commit }, { id, ...data }) => {
  commit(
    'UPDATE_CRITERION',
    await axios.patch(`/api/criteria/${id}`, { ...data })
  );
}

export const deleteCriterion = async ({ commit }, id) => {
  commit(
    'DELETE_CRITERION',
    await axios.delete(`/api/criteria/${id}`)
  );
}

// Проекты
export const loadProjectList = async ({ commit }, { nominationId, page }) => {
  commit(
    'LOAD_PROJECT_LIST', 
    await axios.get('/api/projects', {
      params: {
        nomination_id: nominationId,
        page,
      }
    })
  );
}

export const loadProject = async ({ commit }, id) => {
  commit(
    'LOAD_PROJECT', 
    await axios.get(`/api/projects/${id}`)
  );
}

export const createProject = async ({ commit }, data) => {
  commit(
    'CREATE_PROJECT', 
    await axios.post(`/api/projects`, data)
  );
}

export const updateProject = async ({ commit }, { id, ...data }) => {
  commit(
    'UPDATE_PROJECT',
    await axios.patch(`/api/projects/${id}`, { ...data })
  );
}

export const deleteProject = async ({ commit }, id) => {
  commit(
    'DELETE_PROJECT',
    await axios.delete(`/api/projects/${id}`)
  );
}

// Пользователи
export const loadUserList = async ({ commit }, { page }) => {
  commit(
    'LOAD_USER_LIST', 
    await axios.get('/api/users', {
      params: { page }
    })
  );
}

export const createUser = async ({ commit }, data) => {
  commit(
    'CREATE_USER',
    await axios.post(`/api/users`, { ...data })
  );
}

export const updateUser = async ({ commit }, { id, ...data }) => {
    commit(
      'UPDATE_USER',
      await axios.patch(`/api/users/${id}`, { ...data })
    );
}

export const deleteUser = async ({ commit }, id) => {
    commit(
      'DELETE_USER',
      await axios.delete(`/api/users/${id}`)
    );
}

// Оценки
export const loadRating = async ({ commit }, { projectId, nominationId }) => {
  commit(
    'LOAD_RATING', 
    await axios.get(`/api/projects/${projectId}/rating?nomination_id=${nominationId}`)
  );
}

export const saveRating = async ({ commit }, { projectId, nominationId, ratings }) => {
  commit(
    'SAVE_RATING',
    await axios.post(`/api/projects/${projectId}/rating`, {
      ratings,
      nomination_id: nominationId
    })
  );
}

// Аналитика
export const loadStats = async ({ commit }) => {
  commit(
    'LOAD_STATS', 
    await axios.get(`/api/stats`)
  );
}

// Модерация
export const saveModeration = async ({ commit }, { projectId, nominationId, comment, status }) => {
  commit(
    'SAVE_MODERATION',
    await axios.post(`/api/projects/${projectId}/moderation`, {
      nomination_id: nominationId,
      comment,
      status
    })
  );
}

// Настройки
export const loadSettings = async ({ commit }) => {
  commit(
    'LOAD_SETTINGS', 
    await axios.get(`/api/settings`)
  );
}

export const saveSettings = async ({ commit }, data) => {
  commit(
    'SAVE_SETTINGS',
    await axios.post(`/api/settings`, { ...data })
  );
}