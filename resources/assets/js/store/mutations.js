import Vue from 'vue';

export default {
  // Номинации
  LOAD_NOMINATION_LIST (state, response) {
    if (response.data.current_page) {
      state.nominationList = response.data.data;
      state.pagination.nominationList.currentPage = response.data.current_page;
      state.pagination.nominationList.total = response.data.total;
    } else {
      state.nominationList = response.data;
    }
  },
  LOAD_NOMINATION (state, response) {
    state.nomination = response.data;
  },
  CREATE_NOMINATION(state, response) {
    state.nominationList = [...state.nominationList, response.data];
  },
  UPDATE_NOMINATION(state, response) {
    let index = state.nominationList.findIndex(
      item => item.id === parseInt(response.data.id)
    );
    Vue.set(state.nominationList, index, response.data)
  },
  DELETE_NOMINATION(state, response) {
    let index = state.nominationList.findIndex(
      item => item.id === response.data
    );
    state.nominationList.splice(index, 1);
  },
  // Критерии
  LOAD_CRITERION_LIST (state, response) {
    if (response.data.current_page) {
      state.criterionList = response.data.data;
      state.pagination.criterionList.currentPage = response.data.current_page;
      state.pagination.criterionList.total = response.data.total;
    } else {
      state.criterionList = response.data;
    }
  },
  CREATE_CRITERION(state, response) {
    state.criterionList = [...state.criterionList, response.data];
  },
  UPDATE_CRITERION(state, response) {
    let index = state.criterionList.findIndex(
      item => item.id === parseInt(response.data.id)
    );
    Vue.set(state.criterionList, index, response.data)
  },
  DELETE_CRITERION(state, response) {
    let index = state.criterionList.findIndex(
      item => item.id === response.data
    );
    state.criterionList.splice(index, 1);
  },
  // Проекты
  LOAD_PROJECT_LIST (state, response) {
    if (response.data.current_page) {
      state.projectList = response.data.data;
      state.pagination.projectList.currentPage = response.data.current_page;
      state.pagination.projectList.total = response.data.total;
    } else {
      state.projectList = response.data;
    }
  },
  LOAD_PROJECT (state, response) {
    state.project = response.data;
  },
  CREATE_PROJECT(state, response) {
    state.projectList = [...state.projectList, response.data];
  },
  UPDATE_PROJECT(state, response) {
    let index = state.projectList.findIndex(
      item => item.id === parseInt(response.data.id)
    );
    Vue.set(state.projectList, index, response.data)
  },
  DELETE_PROJECT(state, response) {
    let index = state.projectList.findIndex(
      item => item.id === response.data
    );
    state.projectList.splice(index, 1);
  },
  // Пользователи
  LOAD_USER_LIST (state, response) {
    if (response.data.current_page) {
      state.userList = response.data.data;
      state.pagination.userList.currentPage = response.data.current_page;
      state.pagination.userList.total = response.data.total;
    } else {
      state.userList = response.data;
    }
  },
  CREATE_USER(state, response) {
    state.userList = [...state.userList, response.data];
  },
  UPDATE_USER(state, response) {
    let index = state.userList.findIndex(
      item => item.id === parseInt(response.data.id)
    );
    Vue.set(state.userList, index, response.data)
  },
  DELETE_USER(state, response) {
    let index = state.userList.findIndex(
      item => item.id === parseInt(response.data)
    );
    state.userList.splice(index, 1);
  },
  // Оценки
  LOAD_RATING (state, response) {
    state.rating = response.data.rating;
    state.project.completed = response.data.completed;
  },
  SAVE_RATING (state, response) {
    let edit_mode = state.project.completed;
    state.project.completed = true;
    state.nomination.next_incompleted_project_id = response.data.next_incompleted_project_id;
    if (!edit_mode) {
      state.nomination.completed_projects_count += 1;
    }
  },
  // Модерация
  SAVE_MODERATION (state, response) {
    state.project.moderated = true;
    state.nomination.completed_projects_count += 1;
    state.nomination.next_unmoderated_project_id = response.data.next_unmoderated_project_id;
  },
  // Аналитика
  LOAD_STATS(state, response) {
    state.stats = response.data;
  },
  // Настройки
  LOAD_SETTINGS (state, response) {
    state.settings = response.data;
  },
  SAVE_SETTINGS (state, response) {
    state.settings = response.data;
  },
};
