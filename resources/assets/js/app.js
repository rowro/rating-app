import Vue from 'vue';
import VueRouter from 'vue-router';
import ElementUI from 'element-ui';

import VueYouTubeEmbed from 'vue-youtube-embed';
import vClickOutside from 'v-click-outside';

import Logo from 'Components/Logo.vue';
import UserLinks from 'Components/UserLinks.vue';

import store from './store/index';
import routes from './router';
import './bootstrap';

Vue.mixin({
  methods: {
    checkRole: function (...roles) {
      const user = this.$store.state.user;
      if (Object.keys(user).length !== 0 && user.role) {
        return roles.some(role => role === this.$store.state.user.role.name);
      }
      return false;
    }
  }
})

Vue.use(VueRouter);
Vue.use(ElementUI);
Vue.use(VueYouTubeEmbed);
Vue.use(vClickOutside);

Vue.component('logo', Logo);
Vue.component('user-links', UserLinks);

const router = new VueRouter({
  routes,
  mode: 'history'
});

const defaultRoutes = [
  { role: 'admin', path: '/admin'},
  { role: 'judge', path: '/nominations'},
  { role: 'moderator', path: '/nominations'},
  { role: 'supervisor', path: '/dashboard'},
];

router.beforeEach((to, from, next) => {
  const user = router.app.$store.state.user;
  const isRegistrationFormActive = router.app.$store.state.settings.reception;

  const checkUserRole = roles => roles.some(
    role => role === user.role.name || role === '*'
  );

  if (to.path === '/login' && user.role) {
    let redirectPath = defaultRoutes.find(
      route => route.role === user.role.name
    );
    next({ 
      path: (redirectPath) ? redirectPath.path : '/'
    });
    return;
  }

  if (to.path === '/registration' && !isRegistrationFormActive) {
    next('/');
    return;
  }

  if (to.meta.requireAuth) {
    // Проверка авторизации
    if (Object.keys(user).length === 0) {
      next('/login');
    } else {
      if (!checkUserRole(to.meta.roles)) {
        let redirectPath = defaultRoutes.find(
          route => route.role === user.role.name
        );

        next({ 
          path: (redirectPath) ? redirectPath.path : '/'
        });
      } else {
        next();
      }
    }
  } else {
    next();
  }
});

new Vue({
  el: '#app',
  store,
  router,
});
