import Home from './views/Home.vue';
import RegistrationForm from './views/RegistrationForm.vue';
import Dashboard from './views/Dashboard.vue';
import NominationList from './views/NominationList.vue';
import ProjectView from './views/ProjectView.vue';
import AdminHome from './views/admin/AdminHome.vue';
import AdminNominations from './views/admin/AdminNominations.vue';
import AdminCriteria from './views/admin/AdminCriteria.vue';
import AdminProjects from './views/admin/AdminProjects.vue';
import AdminUsers from './views/admin/AdminUsers.vue';
import AdminSettings from './views/admin/AdminSettings.vue';
import LoginForm from './views/LoginForm.vue';
import PageNotFound from './views/PageNotFound.vue';

export default [
  { 
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      requireAuth: false,
      roles: ['*']
    }
  },
  { 
    path: '/registration',
    name: 'registration',
    component: RegistrationForm,
    meta: {
      requireAuth: false,
      roles: ['*']
    }
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    meta: {
      requireAuth: true,
      roles: ['supervisor']
    }
  },
  { 
    path: '/nominations',
    name: 'nominations',
    component: NominationList,
    meta: {
      requireAuth: true,
      roles: ['judge', 'moderator']
    }
  },
  {
    path: '/nominations/:nominationId/projects/:projectId',
    name: 'projectView',
    component: ProjectView,
    meta: {
      requireAuth: true,
      roles: ['judge', 'moderator']
    }
  },
  {
    path: '/admin',
    name: 'admin.home',
    component: AdminHome,
    meta: {
      requireAuth: true,
      roles: ['admin']
    }
  },
  {
    path: '/admin/nominations',
    name: 'admin.nominations',
    component: AdminNominations,
    meta: {
      requireAuth: true,
      roles: ['admin']
    }
  },
  { 
    path: '/admin/criteria',
    name: 'admin.criteria',
    component: AdminCriteria,
    meta: {
      requireAuth: true,
      roles: ['admin']
    }
  },
  {
    path: '/admin/projects',
    name: 'admin.projects',
    component: AdminProjects,
    meta: {
      requireAuth: true,
      roles: ['admin']
    }
  },
  {
    path: '/admin/users',
    name: 'admin.users',
    component: AdminUsers,
    meta: {
      requireAuth: true,
      roles: ['admin']
    }
  },
  {
    path: '/admin/settings',
    name: 'admin.settings',
    component: AdminSettings,
    meta: {
      requireAuth: true,
      roles: ['admin']
    }
  },
  {
    path: '/login',
    name: 'login',
    component: LoginForm
  },
  {
    path: '*',
    name: '404',
    component: PageNotFound
  }
];
