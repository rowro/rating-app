<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>{{ $settings->title }}</title>
  <meta name="description" content="{{ $settings->description }}"> 
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
  <div id="app">
    @yield('content')
  </div>
  <script>
    window.App = {
      initialState: {!! isset($initial_state) ? json_encode($initial_state) : '{}' !!}
    };
  </script>
  <script src="{{ mix('js/manifest.js') }}"></script>
  <script src="{{ mix('js/vendor.js') }}"></script>
  <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
