@extends('layouts.app')

@section('content')
  <main class="main">
    <router-view></router-view>
  </main>
@endsection
