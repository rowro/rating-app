<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'grant-demo.ru');

// Project repository
set('repository', 'git@gitlab.com:Pro100Wolf/rating-app.git');

set('http_user', 'j903673h');
set('bin/npm', '/home/j/j903673h/.local/bin/npm');

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts

host('j903673h@j903673h.beget.tech')
    ->set('deploy_path', '~/{{application}}');
    
// Tasks

task('passport:keys', function () {
    run('cd {{release_path}}');
    run('{{bin/php}} {{release_path}}/artisan passport:keys');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');
before('deploy:symlink', 'passport:keys');
