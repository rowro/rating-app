<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('projects/{id}/rating', 'ProjectController@showRating');
Route::post('projects/{id}/rating', 'ProjectController@saveRating');
Route::post('projects/{id}/moderation', 'ProjectController@saveModeration');
Route::apiResource('projects', 'ProjectController');

Route::apiResource('criteria', 'CriterionController');

Route::get('nominations/short', 'NominationController@getShortList');
Route::get('nominations/{id}/criteria', 'NominationController@showCriteria');
Route::apiResource('nominations', 'NominationController');

Route::apiResource('users', 'UserController');

Route::get('stats', 'StatsController@index');

Route::get('settings', 'SettingController@index');
Route::post('settings', 'SettingController@save');
